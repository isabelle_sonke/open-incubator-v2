<?php get_header();

function removeEmpty($var) {
    return !($var == "");
}

$pagename = $_SERVER['REQUEST_URI'];
$pagename = explode("/", $pagename);
$pagename = array_filter($pagename, "removeEmpty");
$pagename = end($pagename);

$args = array(
    'post_type' => 'startUp',
    'tax_query' => array(
        array(
            'taxonomy' => 'industries',
            'field'    => 'slug',
            'terms'    => $pagename,
        ),
    ),
);

$query = new WP_Query( $args );
?>
<div class="content"> <?php
    if (have_posts($query)) {
        while (have_posts($query)) {
            the_post();
            ?>
            <h3><?php the_title(); ?></h3>
            <?php
        }
    }
        
        ?>
</div>

<?php get_footer();?>