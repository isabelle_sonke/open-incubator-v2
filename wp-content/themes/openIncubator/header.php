<!doctype html>
<header>
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php wp_head() ?>
</header>
<html>
	<body> 

<div class="menuWrap"> 
	<?php
	wp_nav_menu( $args = array(
		'menu'              => 'Main Menu', //name of the menu set in WP to display
		'theme_location'	=>  'header_menu', //use the menu location defined in functions.php
		'fallback_cb'       => false, // dont fallback on standard wp menu if it fails
		));
	?>
	<div class="secondaryMenu">
	<?php
	if(get_user_role() == 'administrator') {
		wp_nav_menu( $args = array(
			'menu'              => 'Admin menu', //name of the menu set in WP to display
			'theme_location'	=>  'admin_menu', //use the menu location defined in functions.php
			'fallback_cb'       => false, // dont fallback on standard wp menu if it fails
			));
	}
	if(get_user_role() == 'subscriber') {
		wp_nav_menu( $args = array(
			'menu'              => 'Admin menu', //name of the menu set in WP to display
			'theme_location'	=>  'admin_menu', //use the menu location defined in functions.php
			'fallback_cb'       => false, // dont fallback on standard wp menu if it fails
			));
	}
	?>
	</div>
	
</div>

