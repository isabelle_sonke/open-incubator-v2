<?php
add_theme_support( 'post-thumbnails' );

//function to define the type of custom Case post
function customPostTypes() {

    //array of the texts used in the admin menu
    $labels = array(
        'name'               => _x( 'Start-ups', 'post type general name' ),
        'singular_name'      => _x( 'Start-Up', 'post type singular name' ),
        'add_new'            => _x( 'Add New', 'Start-Up' ),
        'add_new_item'       => __( 'Add New Start-Up' ),
        'edit_item'          => __( 'Edit Start-Up' ),
        'new_item'           => __( 'New Start-Up' ),
        'all_items'          => __( 'All Start-Ups' ),
        'view_item'          => __( 'View Start-Ups' ),
        'search_items'       => __( 'Search Start-Ups' ),
        'not_found'          => __( 'No Start-Ups found' ),
        'not_found_in_trash' => __( 'No Start-Ups found in the Trash' ),
        'menu_name'          => 'Start-ups'
    );

    //array of post type properties
    $args = array(
        'labels'        => $labels,
        'description'   => 'All Start-ups using the Open-Incubator',
        'public'        => true,
        'menu_position' => 5,
        'supports'      => array('editor', 'title', 'thumbnail', 'custom-fields', 'page-attributes' ),
        'has_archive'   => false,
        'taxonomies'    => array( 'industries' ),

    );
    register_post_type( 'startUp', $args );
}

//adds the custom post function to wordpress on init
add_action( 'init', 'customPostTypes' );

function registerMenus() {

    //registers the main menu for use in the header
    register_nav_menus( array(
        'header_menu'   => 'Header menu',
        'footer_menu'   => 'Footer menu',
        'customer_menu' => 'Customer Menu',
        'admin_menu'    => 'Admin Menu'
    ));
}
add_action('init', 'registerMenus');

//add theme options pages
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Theme Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-settings',
		'capability'	=> 'administrator',
		'redirect'		=> false
	));
}

function registerTaxonomy() {
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Industries', 'textdomain'),
        'singular_name'     => _x( 'Industry', 'textdomain'),
        'search_items'      => __( 'Search Industries', 'textdomain'),
        'all_items'         => __( 'All Industries', 'textdomain'),
        'parent_item'       => __( 'Parent Industry', 'textdomain'),
        'parent_item_colon' => __( 'Parent Industry:', 'textdomain'),
        'edit_item'         => __( 'Edit Industry', 'textdomain'),
        'update_item'       => __( 'Update Industry', 'textdomain'),
        'add_new_item'      => __( 'Add New Industry', 'textdomain'),
        'new_item_name'     => __( 'New Industry name', 'textdomain'),
        'menu_name'         => __( 'Industries'),
    );
 
    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'show_in_nav_menus' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'industries' ),
    );
 
    register_taxonomy( 'industries', array( 'industries' ), $args );
}
add_action( 'init', 'registerTaxonomy', 0 );

//registers stylesheet
function enqueueStyle() {
	wp_enqueue_style( 'style.css', get_stylesheet_uri());
}
add_action( 'wp_enqueue_scripts', 'enqueueStyle' );

add_action( 'wp_head', 'get_user_role'); 

function get_user_role() {
	
 if( is_user_logged_in() ) { // check if there is a logged in user 
	 
	 $user = wp_get_current_user(); // getting & setting the current user 
	 $roles = ( array ) $user->roles; // obtaining the role 
	 
		return $roles[0]; // return the role for the current user 
	 
	 } else {
		 
		return array(); // if there is no logged in user return empty array  
	 
	 }
}
add_filter("walker_nav_menu_start_el", "filter_menu_items",10,4);
function filter_menu_items($output, $item, $depth, $args) {
    if ('header-menu' == $args->theme_location && $depth === 0) {
        if(in_array("menu-item-has-children", $item->classes)) {
            $output = strip_tags($output);
        }
        
    }
    return $output;
}